# React-Native demo application. Provides integration examples with connatix-player-sdk-react-native

Run the following commands in the terminal in order to install dependencies:
```
yarn
cd ios
pod install
```

In order to run the application on Android, run the following command in the terminal:
```
npx react-native run-android
```

In order to run the application on iOS, run the following command in the terminal:
```
npx react-native run-ios
```
